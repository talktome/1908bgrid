all: pic.xbm
	xsetroot -bg green -bitmap pic.xbm

pic.svg: ./gen-svg
	./gen-svg > pic.svg

pic.xbm: pic.svg
	convert pic.svg pic.xbm

clean:
	rm pic.svg pic.xbm
