# Bgrid

Paints a 6x6 grid on the X root window. Each grid contains one [0-9] or [a-z] character.

Useful for aligning windows.

Every possible grid (of grids) can be described by two characters, or two keys on an alphanumeric keyboard.

Usage:

	make
